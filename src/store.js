import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex, Axios)

/**
 * fn for make products list shortly
 * if product don`t have own image, there is set default
 * */
function helperArray(el) {
    let outArr = []

    for (let i = 0, len = el.length; i < len; i++) {
        let arr = {}
        arr.id = el[i].gsx$id.$t
        arr.name = el[i].gsx$name.$t
        arr.cost = el[i].gsx$cost.$t
        arr.image = el[i].gsx$image.$t || 'http://sunshell.net/wp-content/themes/arkahost/assets/images/default.png'
        outArr[i] = arr
    }
    return outArr
}

class Product {
    constructor(prod) {
        this.id = prod.id
        this.name = prod.name
        this.cost = prod.cost
        this.image = prod.image
        this.counter = 1
        this.amount = prod.cost
    }

}

function saveStorage(arr) {
    try {
        let card = JSON.stringify(arr)
        sessionStorage.setItem('card', card)
    } catch (err) {
        sessionStorage.removeItem('card')
    }
}

function loadFromStorageArr() {
    try {
        let card = JSON.parse(sessionStorage.getItem('card'))
        if (card === null) return undefined
        else return sortArrFromObj(card)
    } catch (err) {
        sessionStorage.removeItem('card')
    }
}

function loadFromStorage() {
    try {
        let card = JSON.parse(sessionStorage.getItem('card'))
        if (card === null) return undefined
        else return card
    } catch (err) {
        sessionStorage.removeItem('card')
    }
}

// fn for return count elements in card
function loadCounterFromStorage() {
    try {
        let card = JSON.parse(sessionStorage.getItem('card'))
        if (card === null) return undefined
        else {
            return Object.keys(card).length
        }
    } catch (err) {
        sessionStorage.removeItem('card')
    }
}

// fn for return total price of all selected items
function loadTotalFromStorage() {
    try {
        let card = JSON.parse(sessionStorage.getItem('card'))
        if (card === null) return undefined
        else {
            let total = Object.values(card).map(item => item.data.counter * item.data.cost)
                .reduce((sum, cost) => sum + cost)
            return total
        }
    } catch (err) {
        sessionStorage.removeItem('card')
    }
}

// fn for amount price of all selected items
function getTotal(obj) {
    let total = 0

    if (Object.keys(obj).length > 0) {
        total = Object.values(obj).map(item => item.data.counter * item.data.cost)
            .reduce((sum, cost) => sum + cost)
    }

    return total
}

function sortByField(field) {
    return function (a, b) {
        return a[field] < b[field] ? 1 : -1
    }
}

function sortArrFromObj(obj) {
    return Object.values(obj).sort(sortByField('added'))
}

// url public Google sheet
const URL = 'https://spreadsheets.google.com/feeds/list/1k1KsTac2Aqb9yiVnb7HLzYJJ3_DYD6W2_D-ZYCyAB48/od6/public/values?alt=json'

export default new Vuex.Store({
    state: {
        loading: false,
        products: [],
        selectedItems: loadFromStorageArr() || [],
        totalSelected: loadCounterFromStorage() || 0,
        total: loadTotalFromStorage() || 0,
        helperObject: loadFromStorage() || {}
    },
    mutations: {
        loading: function (state, payload) {
            state.loading = payload
        },
        setProductsList: function (state, payload) {
            state.products = payload
        },
        addSelectedProduct: function (state, payload) {
            state.selectedItems = payload
        },
        addToHelperObject: function (state, payload) {
            state.helperObject = payload
            saveStorage(state.helperObject)
        },
        selectedItemsCounter: function (state, payload) {
            state.totalSelected = payload
        },
        total: function (state, payload) {
            state.total = payload
        }
    },
    actions: {
        /**
         * fn for get all product from Google API
         * @var products        -> simplified Array for products list
         * @func helperArray    -> function for create data more shortly, entry arr, return array
         */
        getProductsFromAPI: async function ({commit}) {
            commit('loading', true)
            await Axios.get(URL).then(response => {
                let products = helperArray(response.data.feed.entry)
                commit('setProductsList', products)
                commit('loading', false)
            })
        },

        /**
         * fn to set object card list of all selected products
         * @param commit
         * @param state
         * @param id        -> id of product which was select
         * @var card        -> obj of all selected products
         * @var time        -> variable for sort array to know which product was last
         * @var counter     -> counter selected products in card
         * @fn getTotal     -> count total price of all selected items
         */
        selectedProduct: function ({commit, state}, id) {
            let card = state.helperObject,
                time = new Date().getTime()

            // if product is not in the selected list
            // find it at all product list and create it in card
            // set time when it was added
            if (card[id] === undefined) {
                let prod = state.products.find(item => item.id === id)
                card[id] = {
                    data: new Product(prod),
                    added: time
                }
            }
            // if product in the selected list
            // increment counter && set time when it was added
            else {
                card[id].data.counter++
                card[id].data.amount = card[id].data.counter * card[id].data.cost
                card[id].added = time
            }

            let counter = Object.keys(card).length,
                arr = sortArrFromObj(card)

            commit('selectedItemsCounter', counter)
            commit('addSelectedProduct', arr)
            commit('addToHelperObject', card)
            commit('total', getTotal(card))
        },
        incr_decr: function ({commit, state}, {method, id}) {
            let card = state.helperObject
            if (method === 'increment') {
                card[id].data.counter++
            } else {
                card[id].data.counter--
            }

            card[id].data.amount = card[id].data.counter * card[id].data.cost

            let arr = sortArrFromObj(card)

            commit('addSelectedProduct', arr)
            commit('addToHelperObject', card)
            commit('total', getTotal(card))
        },

        /**
         * fn for delete item from card if it`s last item of count
         * if item is last in list clear storage
         * @var id          -> id product which have to delete
         * @fn getTotal     -> count total price of all selected items
         */
        deleteItemFromCard: function ({commit, state}, id) {
            let card = state.helperObject
            delete card[id]

            let total = 0,
                counter = Object.keys(card).length,
                arr = []
            if (counter > 0) {
                total = getTotal(card)
                arr = sortArrFromObj(card)
                commit('addSelectedProduct', arr)
                commit('addToHelperObject', card)
            } else {
                sessionStorage.removeItem('card')
            }

            commit('selectedItemsCounter', counter)
            commit('total', total)
            return arr
        },
        deleteCard: function ({commit}) {
            commit('selectedItemsCounter', 0)
            commit('addSelectedProduct', [])
            commit('addToHelperObject', {})
            commit('total', 0)
            sessionStorage.removeItem('card')
        }
    },
    getters: {
        loading: state => state.loading,
        products: state => state.products,
        totalSelected: state => state.totalSelected,
        selectedItems: state => state.selectedItems,
        total: state => state.total,
        helperObject: state => state.helperObject
    }
})
