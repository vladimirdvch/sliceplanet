import Vue from 'vue'
import Router from 'vue-router'
import MainPage from './components/Index'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: 'active-router', // global class for active route
    routes: [
        {
            path: '/',
            name: 'index',
            component: MainPage
        },
        {
            path: '/card',
            name: 'card',
            component: () => import('./components/Card')
        },
        {
            path: '/registration',
            name: 'registration',
            component: () => import('./components/Auth')
        },
        {
            path: '*', // 404 page
            component: () => import('./components/ErrorPage')
        }
    ],
    scrollBehavior(to, from) {
        return {x: 0, y: 0}
    }
})
