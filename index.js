const express = require('express')
const path = require('path')
const app = express()
const port = process.env.PORT || 5000

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('./dist'))
    app.get('*', (req, res) => {
        res.sendFile(
            path.resolve(__dirname, 'dist', 'index.html')
        )
    })
}

app.listen(port, () => console.log(`server start on ${port}`))